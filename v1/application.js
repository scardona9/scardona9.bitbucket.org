(function () {

  "use strict";

  var React = window.React
    , ReactCSSTransitionGroup = React.addons.CSSTransitionGroup
    , USUARIOS = null
    , PRODUCTO_ACTUAL = null
    , PRODUCTOS = null
    , MainHeader = null
    , Producto = null
    , Productos = null
    , Usuarios = null
    , ModificacionUsuario = null
    , BajaUsuario = null
    , AltaUsuario = null
    , Main = null
    , Application = null
    , guardarUsuario = null
    , contrasenaValida = null;

  USUARIOS = [
    {
      nombre: "John",
      apellido: "Doe",
      contrasena: "aB123456",
      confirmacion_contrasena: "aB123456",
      direccion: "Cuareim 1451",
      telefono: "123456789",
      email: "ie@ort.edu.uy"
    },
    {
      nombre: "John",
      apellido: "Doe 2",
      contrasena: "aB123456",
      confirmacion_contrasena: "aB123456",
      direccion: "Cuareim 1451",
      telefono: "123456789",
      email: "ie@ort.edu.uy"
    }
  ];

  PRODUCTOS = [
    {
      nombre: "Side-by-side General Electric 23 Inoxidable",
      codigo: "001",
      rubro: "Refrigeracion",
      tipo: "Side-by-side",
      precio: "USD 1890.00"
    },
    {
      nombre: "Side-by-side General Electric GKCT2DEBFGP Plata",
      codigo: "002",
      rubro: "Refrigeracion",
      tipo: "Side-by-side",
      precio: "USD 1790.00"
    },
    {
      nombre: "Refrigerador Samsung RT51KTPN",
      codigo: "003",
      rubro: "Refrigeracion",
      tipo: "Frio Seco",
      precio: "USD 1299.00"
    },
    {
      nombre: "Refrigerador LG GM-S 582ULV Silver",
      codigo: "004",
      rubro: "Refrigeracion",
      tipo: "Frio Seco",
      precio: "USD 1299.00"
    },
    {
      nombre: "Microondas LG MJ3281BC Solarseries",
      codigo: "005",
      rubro: "Cocinas y hornos",
      tipo: "Microondas",
      precio: "USD 499.00"
    },
    {
      nombre: "Microondas Samsung MC28H5135",
      codigo: "006",
      rubro: "Cocinas y hornos",
      tipo: "Microondas",
      precio: "USD 299.00"
    },
    {
      nombre: "Microondas digital con grill LG MH7083AAK",
      codigo: "007",
      rubro: "Cocinas y hornos",
      tipo: "Microondas",
      precio: "USD 249.00"
    },
    {
      nombre: "Lavarropa LG TS1604DPH",
      codigo: "008",
      rubro: "Lavado y secado",
      tipo: "Lavarropa",
      precio: "USD 1299.00"
    },
    {
      nombre: "Lavarropa Samsung WA16F7",
      codigo: "009",
      rubro: "Lavado y secado",
      tipo: "Lavarropa",
      precio: "USD 899.00"
    },
    {
      nombre: "Lavarropa James WMT 680",
      codigo: "010",
      rubro: "Lavado y secado",
      tipo: "Lavarropa",
      precio: "USD 299.00"
    }
  ];

  contrasenaValida = function (contrasena) {
    return true;
  };

  guardarUsuario = function (datos, indice) {
    var errores = [];

    if (datos.nombre.trim().length === 0) {
      errores.push("Nombre inválido.");
    }

    if (datos.apellido.trim().length === 0) {
      errores.push("Apellido inválido.");
    }

    if (datos.contrasena.trim().length === 0) {
      errores.push("Contraseña inválida.");
    }

    if (!contrasenaValida(datos.contrasena.trim())) {
      errores.push("Contraseña inválida.");
    }

    if (datos.confirmacion_contrasena.trim().length === 0) {
      errores.push("Confirmación de contraseña inválida.");
    }

    if (datos.contrasena.trim() !== datos.confirmacion_contrasena.trim()) {
      errores.push("Confirmación de contraseña inválida.");
    }

    if (datos.direccion.trim().length === 0) {
      errores.push("Dirección inválida.");
    }

    if (datos.telefono.trim().length === 0) {
      errores.push("Teléfono celular inválido.");
    }

    if (datos.email.trim().length === 0) {
      errores.push("Mail inválido.");
    }

    if (errores.length > 0) {
      alert(errores.join(" "));
    }
    else if (typeof indice === "undefined") {
      USUARIOS.push(datos);
      alert("Usuario ingresado correctamente.");
    }
    else {
      USUARIOS[indice] = datos;
      alert("Usuario modificado correctamente.");
    }

    return errores.length === 0;
  };

  /* MainHeader ***************************************************************/
  MainHeader = React.createClass({
    render: function () {
      return React.createElement(
        "header",
        { className: "mainHeader" },
        React.createElement(
          "a",
          { className: "logo", href: "/" },
          React.createElement(
            "img",
            { className: "logo-img", src: "../logo.png" }
          )
        )
      );
    }
  });

  /* Main *********************************************************************/
  Main = React.createClass({
    render: function () {
      return React.createElement(
        "section",
        { className: "mainContent" },
        React.createElement(
          "div",
          { className: "navList" },
          React.createElement(
            "a",
            { className: "navList-item button button--primary", onClick: this._buscarProducto },
            "Productos"
          ),
          React.createElement(
            "a",
            { className: "navList-item button button--primary", onClick: this._usuarios },
            "Gestión de usuarios"
          )
        )
      );
    },
    _buscarProducto: function (event) {
      event.preventDefault();

      this.props.onViewChange("productos");
    },
    _usuarios: function (event) {
      event.preventDefault();

      this.props.onViewChange("usuarios");
    }
  });

  /* Producto *****************************************************************/
  Producto = React.createClass({
    render: function () {
      var producto = PRODUCTO_ACTUAL
        , contenido = null
        , descripcion = null;

      descripcion = [
        "Nombre: " + producto.nombre,
        "Código: " + producto.codigo,
        "Rubro: " + producto.rubro,
        "Tipo: " + producto.tipo
      ].join(". ");

      contenido = React.createElement(
        "div",
        { className: "detalle" },
        React.createElement(
          "img",
          { className: "detalle-img", src: "../" + producto.codigo + ".png" }
        ),
        React.createElement(
          "div",
          { className: "detalle-titulo" },
          producto.nombre
        ),
        React.createElement(
          "div",
          { className: "detalle-descripcion" },
          descripcion
        )
      );

      return React.createElement(
        "div",
        {},
        React.createElement(
          "div",
          { className: "pageHeader" },
          "Productos",
          React.createElement(
            "a",
            { className: "pageHeader--secondaryItem", onClick: this._clickBack },
            "Atrás"
          )
        ),
        contenido
      );
    },
    _clickBack: function (event) {
      event.preventDefault();

      this.props.onBack();
    }
  });

  /* Productos ****************************************************************/
  Productos = React.createClass({
    getInitialState: function () {
      return {
        tipo: "nombre",
        texto: ""
      };
    },
    render: function () {
      var PRODUCTO_ACTUAL = this.state.producto
        , contenido = null;

      contenido = React.createElement(
        "div",
        { className: "container" },
        React.createElement(
          "input",
          {
            type: "text",
            className: "input",
            value: this.state.texto,
            onChange: this._changeTexto
          }
        ),
        React.createElement(
          "div",
          {},
          React.createElement(
            "label",
            { className: "label", htmlFor: "nombre" },
            React.createElement(
              "input",
              {
                type: "radio",
                name: "tipo",
                value: "nombre",
                id: "nombre",
                checked: this.state.tipo === "nombre",
                onChange: this._changeTipo
              }
            ),
            "Nombre"
          )
        ),
        React.createElement(
          "div",
          {},
          React.createElement(
            "label",
            { className: "label", htmlFor: "codigo" },
            React.createElement(
              "input",
              {
                type: "radio",
                name: "tipo",
                value: "codigo",
                id: "codigo",
                checked: this.state.tipo === "codigo",
                onChange: this._changeTipo
              }
            ),
            "Codigo de producto"
          )
        ),
        React.createElement(
          "div",
          {},
          React.createElement(
            "label",
            { className: "label", htmlFor: "rubro" },
            React.createElement(
              "input",
              {
                type: "radio",
                name: "tipo",
                value: "rubro",
                id: "rubro",
                checked: this.state.tipo === "rubro",
                onChange: this._changeTipo
              }
            ),
            "Rubro"
          )
        ),
        React.createElement(
          "div",
          {},
          React.createElement(
            "label",
            { className: "label", htmlFor: "tipo" },
            React.createElement(
              "input",
              {
                type: "radio",
                name: "tipo",
                value: "tipo",
                id: "tipo",
                checked: this.state.tipo === "tipo",
                onChange: this._changeTipo
              }
            ),
            "Tipo"
          )
        ),
        React.createElement(
          "div",
          { className: "navList" },
          React.createElement(
            "button",
            {
              className: "navList-item button button--primary",
              onClick: this._submitFiltros
            },
            "Buscar"
          )
        )
      );

      return React.createElement(
        "div",
        {},
        React.createElement(
          "div",
          { className: "pageHeader" },
          "Productos",
          React.createElement(
            "a",
            { className: "pageHeader--secondaryItem", onClick: this._clickBack },
            "Atrás"
          )
        ),
        contenido
      );
    },
    componentDidMount: function () {
      this.setState({ producto: null });
    },
    _submitFiltros: function (event) {
      var texto = this.state.texto.toLowerCase()
        , productoEncontrado = false
        , producto = null
        , i = null
        , len = null;

      event.preventDefault();

      for (i = 0, len = PRODUCTOS.length; i < len && !productoEncontrado; i += 1) {
        producto = PRODUCTOS[i];
        productoEncontrado = producto[this.state.tipo].toLowerCase().indexOf(texto) > -1;
      }

      if (productoEncontrado) {
        PRODUCTO_ACTUAL = producto;
        this.props.onViewChange("producto-detalle");
      }
      else {
        alert("No se encontró el producto.");
      }
    },
    _clickBack: function (event) {
      event.preventDefault();

      this.props.onBack();
    },
    _changeTipo: function (event) {
      this.setState({ tipo: event.target.value });
    },
    _changeTexto: function (event) {
      this.setState({ texto: event.target.value });
    }
  });

  /* Modificacion de Usuario **************************************************/
  ModificacionUsuario = React.createClass({
    render: function () {
      var usuarios = []
        , i = null
        , len = null
        , usuario = null;

      for (i = 0, len = USUARIOS.length; i < len; i += 1) {
        usuario = USUARIOS[i];

        usuarios.push(
          React.createElement(
            "option",
            { value: i, key: i },
            [usuario.nombre, usuario.apellido].join(" ")
          )
        );
      };

      return React.createElement(
        "section",
        { className: "mainContent" },
        React.createElement(
          "div",
          { className: "pageHeader" },
          "Modificacion de Usuario",
          React.createElement(
            "a",
            { className: "pageHeader--secondaryItem", onClick: this._clickBack },
            "Atras"
          )
        ),
        React.createElement(
          "form",
          {},
          React.createElement(
            "label",
            { className: "label", htmlFor: "usuario" },
            "Usuario"
          ),
          React.createElement(
            "select",
            { className: "input", ref: "usuario", id: "usuario" },
            usuarios
          ),
          React.createElement(
            "label",
            { className: "label", htmlFor: "nombre" },
            "Nombre"
          ),
          React.createElement(
            "input",
            { className: "input", id: "nombre", ref: "nombre" }
          ),
          React.createElement(
            "label",
            { className: "label", htmlFor: "apellido" },
            "Apellido"
          ),
          React.createElement(
            "input",
            { className: "input", id: "apellido", ref: "apellido" }
          ),
          React.createElement(
            "label",
            { className: "label", htmlFor: "contrasena" },
            "Contraseña"
          ),
          React.createElement(
            "input",
            { className: "input", id: "contrasena", ref: "contrasena" , type: "password"}
          ),
          React.createElement(
            "label",
            { className: "label", htmlFor: "confirmacion_contrasena" },
            "Confirmación de Contraseña"
          ),
          React.createElement(
            "input",
            { className: "input", id: "confirmacion_contrasena", ref: "confirmacion_contrasena", type: "password" }
          ),
          React.createElement(
            "label",
            { className: "label", htmlFor: "direccion" },
            "Dirección"
          ),
          React.createElement(
            "input",
            { className: "input", id: "direccion", ref: "direccion" }
          ),
          React.createElement(
            "label",
            { className: "label", htmlFor: "telefono" },
            "Teléfono"
          ),
          React.createElement(
            "input",
            { className: "input", id: "telefono", ref: "telefono" }
          ),
          React.createElement(
            "label",
            { className: "label", htmlFor: "email" },
            "Email"
          ),
          React.createElement(
            "input",
            { className: "input", id: "email", ref: "email" }
          ),
          React.createElement(
            "div",
            { className: "navList" },
            React.createElement(
              "input",
              {
                type: "submit",
                className: "button button--primary",
                onClick: this._modificarUsuario,
                value: "Aceptar"
              }
            )
          )
        )
      );
    },
    _clickBack: function (event) {
      event.preventDefault();
      this.props.onBack();
    },
    _modificarUsuario: function (event) {
      var indiceUsuario = this.refs.usuario.getDOMNode().value
        , datos = null;

      event.preventDefault();

      datos = {
        nombre: this.refs.nombre.getDOMNode().value,
        apellido: this.refs.apellido.getDOMNode().value,
        contrasena: this.refs.contrasena.getDOMNode().value,
        confirmacion_contrasena: this.refs.confirmacion_contrasena.getDOMNode().value,
        direccion: this.refs.direccion.getDOMNode().value,
        telefono: this.refs.telefono.getDOMNode().value,
        email: this.refs.email.getDOMNode().value
      };

      if (guardarUsuario(datos, indiceUsuario)) {
        this.props.onBack();
      }
    }
  });

  /* Alta de Usuario **********************************************************/
  AltaUsuario = React.createClass({
    render: function () {
      return React.createElement(
        "section",
        { className: "mainContent" },
        React.createElement(
          "div",
          { className: "pageHeader" },
          "Alta de Usuario",
          React.createElement(
            "a",
            { className: "pageHeader--secondaryItem", onClick: this._clickBack },
            "Atras"
          )
        ),
        React.createElement(
          "form",
          {},
          React.createElement(
            "label",
            { className: "label", htmlFor: "nombre" },
            "Nombre"
          ),
          React.createElement(
            "input",
            { className: "input", id: "nombre", ref: "nombre" }
          ),
          React.createElement(
            "label",
            { className: "label", htmlFor: "apellido" },
            "Apellido"
          ),
          React.createElement(
            "input",
            { className: "input", id: "apellido", ref: "apellido" }
          ),
          React.createElement(
            "label",
            { className: "label", htmlFor: "contrasena" },
            "Contraseña"
          ),
          React.createElement(
            "input",
            { className: "input", id: "contrasena", ref: "contrasena", type: "password" }
          ),
          React.createElement(
            "label",
            { className: "label", htmlFor: "confirmacion_contrasena" },
            "Confirmación de Contraseña"
          ),
          React.createElement(
            "input",
            { className: "input", id: "confirmacion_contrasena", ref: "confirmacion_contrasena", type: "password" }
          ),
          React.createElement(
            "label",
            { className: "label", htmlFor: "direccion" },
            "Dirección"
          ),
          React.createElement(
            "input",
            { className: "input", id: "direccion", ref: "direccion" }
          ),
          React.createElement(
            "label",
            { className: "label", htmlFor: "telefono" },
            "Teléfono"
          ),
          React.createElement(
            "input",
            { className: "input", htmlFor: "telefono", ref: "telefono" }
          ),
          React.createElement(
            "label",
            { className: "label", htmlFor: "email" },
            "Email"
          ),
          React.createElement(
            "input",
            { className: "input", id: "email", ref: "email" }
          ),
          React.createElement(
            "div",
            { className: "navList" },
            React.createElement(
              "input",
              {
                type: "submit",
                className: "button button--primary",
                onClick: this._modificarUsuario,
                value: "Aceptar"
              }
            )
          )
        )
      );
    },
    _clickBack: function (event) {
      event.preventDefault();
      this.props.onBack();
    },
    _modificarUsuario: function (event) {
      var datos = null;

      event.preventDefault();

      datos = {
        nombre: this.refs.nombre.getDOMNode().value,
        apellido: this.refs.apellido.getDOMNode().value,
        contrasena: this.refs.contrasena.getDOMNode().value,
        confirmacion_contrasena: this.refs.confirmacion_contrasena.getDOMNode().value,
        direccion: this.refs.direccion.getDOMNode().value,
        telefono: this.refs.telefono.getDOMNode().value,
        email: this.refs.email.getDOMNode().value
      };

      if (guardarUsuario(datos)) {
        this.props.onBack();
      }
    }
  });

  /* Baja Usuario ****************************************************************/
  BajaUsuario = React.createClass({
    render: function () {
      var usuarios = []
        , i = null
        , len = null
        , usuario = null;

      for (i = 0, len = USUARIOS.length; i < len; i += 1) {
        usuario = USUARIOS[i];

        usuarios.push(
          React.createElement(
            "option",
            { value: i, key: i },
            [usuario.nombre, usuario.apellido].join(" ")
          )
        );
      };

      return React.createElement(
        "section",
        { className: "mainContent" },
        React.createElement(
          "div",
          { className: "pageHeader" },
          "Baja de Usuario",
          React.createElement(
            "a",
            { className: "pageHeader--secondaryItem", onClick: this._clickBack },
            "Atras"
          )
        ),
        React.createElement(
          "form",
          {},
          React.createElement(
            "div",
            {},
            React.createElement(
              "label",
              { className: "label", htmlFor: "usuario" },
              "Usuario"
            ),
            React.createElement(
              "select",
              { className: "input", ref: "usuario", id: "usuario" },
              usuarios
            )
          ),
          React.createElement(
            "div",
            { className: "navList" },
            React.createElement(
              "input",
              {
                type: "submit",
                className: "button button--primary",
                onClick: this._eliminarUsuario,
                value: "Eliminar"
              }
            )
          )
        )
      );
    },
    _clickBack: function (event) {
      event.preventDefault();
      this.props.onBack();
    },
    _eliminarUsuario: function (event) {
      event.preventDefault();
      USUARIOS.splice(this.refs.usuario.getDOMNode().value, 1);
      alert("Usuario eliminado correctamente.");
      this.props.onBack();
    }
  });

  /* Usuarios *****************************************************************/
  Usuarios = React.createClass({
    render: function () {
      return React.createElement(
        "section",
        { className: "mainContent" },
        React.createElement(
          "div",
          { className: "navList" },
          React.createElement(
            "a",
            { className: "navList-item button button--primary", onClick: this._altaUsuario },
            "Nuevo usuario"
          ),
          React.createElement(
            "a",
            { className: "navList-item button button--primary", onClick: this._modificacionUsuario },
            "Editar usuario"
          ),
          React.createElement(
            "a",
            { className: "navList-item button button--primary", onClick: this._bajaUsuario },
            "Eliminar Usuario"
          )
        )
      );
    },
    _altaUsuario: function (event) {
      event.preventDefault();
      this.props.onViewChange("alta-usuario");
    },
    _modificacionUsuario: function (event) {
      event.preventDefault();
      this.props.onViewChange("modificacion-usuario");
    },
    _bajaUsuario: function (event) {
      event.preventDefault();
      this.props.onViewChange("baja-usuario");
    }
  });

  /* Application **************************************************************/
  Application = React.createClass({
    getInitialState: function () {
      return {
        view: "main"
      };
    },
    render: function () {
      var view = null;

      switch (this.state.view) {
      case "main":
        view = React.createElement(Main, { onViewChange: this._viewChange, key: "main" });
        break;
      case "productos":
        view = React.createElement(Productos, { onBack: this._back, key: "productos", onViewChange: this._viewChange });
        break;
      case "producto-detalle":
        view = React.createElement(Producto, { onBack: this._back, key: "producto" });
        break;
      case "usuarios":
        view = React.createElement(Usuarios, { onBack: this._back, key: "usuarios", onViewChange: this._viewChange });
        break;
      case "alta-usuario":
        view = React.createElement(AltaUsuario, { onBack: this._back, key: "alta-usuario" });
        break;
      case "modificacion-usuario":
        view = React.createElement(ModificacionUsuario, { onBack: this._back, key: "modificacion-usuario" });
        break;
      case "baja-usuario":
        view = React.createElement(BajaUsuario, { onBack: this._back, key: "baja-usuario" });
        break;
      }

      return React.createElement(
        "div",
        { className: "mobile" },
        React.createElement(MainHeader),
        React.createElement(
          "div",
          { className: "mobile-view" },
          React.createElement(
            ReactCSSTransitionGroup,
            { transitionName: "anim_slideLeft", component: "div", transitionLeave: false },
            view
          )
        )
      );
    },
    _viewChange: function (view) {
      this.setState({ view: view });
    },
    _back: function () {
      this._viewChange("main");
    }
  });

  window.onload = function () {
    React.render(React.createElement(Application), document.body);
  };

})();
